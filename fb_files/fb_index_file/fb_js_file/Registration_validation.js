
function check()
{
	// to check all the blank
	for(i=0;i<Reg.length;i++)
	{
		if(Reg[i].type=="text")
		{
			if(Reg[i].value=="")
			{
				document.getElementById("error_design_format").style.display='block';
				document.getElementById("Name_error").style.display='none';
				document.getElementById("full_Name_error").style.display='none';
				document.getElementById("Email_error").style.display='none';
				document.getElementById("Email_not_match_error").style.display='none';
				document.getElementById("Password_error").style.display='none';
				document.getElementById("Gender_error").style.display='none';
				document.getElementById("Date_error").style.display='none';
				document.getElementById("blank_error").style.display='block';
				Reg[i].focus();
				return false;
			}
			
		}
	}
	
	
	var fnm=Reg.first_name.value;
	for(var index=0;index<fnm.length;index++)
	{
		if(fnm.toUpperCase().charAt(index)<'A' || fnm.toUpperCase().charAt(index)>'Z')
		{
			document.getElementById("error_design_format").style.display='block';
			document.getElementById("blank_error").style.display='none';
			document.getElementById("full_Name_error").style.display='none';
			document.getElementById("Email_error").style.display='none';
			document.getElementById("Email_not_match_error").style.display='none';
			document.getElementById("Password_error").style.display='none';
			document.getElementById("Gender_error").style.display='none';
			document.getElementById("Date_error").style.display='none';
			document.getElementById("Name_error").style.display='block';
			Reg.first_name.focus();
			return false;
			break;
		}
	}
	
	
	if(fnm.length<=1)
	{
		document.getElementById("error_design_format").style.display='block';
		document.getElementById("blank_error").style.display='none';
		document.getElementById("Name_error").style.display='none';
		document.getElementById("Email_error").style.display='none';
		document.getElementById("Email_not_match_error").style.display='none';
		document.getElementById("Password_error").style.display='none';
		document.getElementById("Gender_error").style.display='none';
		document.getElementById("Date_error").style.display='none';
		document.getElementById("full_Name_error").style.display='block';
		Reg.first_name.focus();
		return false;
	}
	
	
	var lnm=Reg.last_name.value;
	for(var index=0;index<lnm.length;index++)
	{
		if(lnm.toUpperCase().charAt(index)<'A' || lnm.toUpperCase().charAt(index)>'Z')
		{
			document.getElementById("error_design_format").style.display='block';
			document.getElementById("blank_error").style.display='none';
			document.getElementById("full_Name_error").style.display='none';
			document.getElementById("Email_error").style.display='none';
			document.getElementById("Email_not_match_error").style.display='none';
			document.getElementById("Password_error").style.display='none';
			document.getElementById("Gender_error").style.display='none';
			document.getElementById("Date_error").style.display='none';
			document.getElementById("Name_error").style.display='block';
			Reg.last_name.focus();
			return false;
			break;
		}
	}
	

	if(lnm.length<=1)
	{
		document.getElementById("error_design_format").style.display='block';
		document.getElementById("blank_error").style.display='none';
		document.getElementById("Name_error").style.display='none';
		document.getElementById("Email_error").style.display='none';
		document.getElementById("Email_not_match_error").style.display='none';
		document.getElementById("Password_error").style.display='none';
		document.getElementById("Gender_error").style.display='none';
		document.getElementById("Date_error").style.display='none';
		document.getElementById("full_Name_error").style.display='block';
		Reg.last_name.focus();
		return false;
	}
	
	
	
		var x=Reg.email.value;
		var atpos=x.indexOf("@");
		var dotpos=x.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
 		{
  			document.getElementById("error_design_format").style.display='block';
			document.getElementById("blank_error").style.display='none';
			document.getElementById("Name_error").style.display='none';
			document.getElementById("full_Name_error").style.display='none';
			document.getElementById("Email_not_match_error").style.display='none';
			document.getElementById("Password_error").style.display='none';
			document.getElementById("Gender_error").style.display='none';
			document.getElementById("Date_error").style.display='none';
			document.getElementById("Email_error").style.display='block';
			Reg.email.focus();
 		 	return false;
 		}
	
		var doname=x.substr(atpos+1,5);
		var donameup=doname.toUpperCase();
		if(donameup!="GMAIL" && donameup!="YAHOO")
		{
			document.getElementById("error_design_format").style.display='block';
			document.getElementById("blank_error").style.display='none';
			document.getElementById("Name_error").style.display='none';
			document.getElementById("full_Name_error").style.display='none';
			document.getElementById("Email_not_match_error").style.display='none';
			document.getElementById("Password_error").style.display='none';
			document.getElementById("Gender_error").style.display='none';
			document.getElementById("Date_error").style.display='none';
			document.getElementById("Email_error").style.display='block';
			Reg.email.focus();
 		 	return false;
		}
	
	
	if(Reg.email.value!=Reg.remail.value)
	{
		document.getElementById("error_design_format").style.display='block';
		document.getElementById("blank_error").style.display='none';
		document.getElementById("Name_error").style.display='none';
		document.getElementById("full_Name_error").style.display='none';
		document.getElementById("Email_error").style.display='none';
		document.getElementById("Password_error").style.display='none';
		document.getElementById("Gender_error").style.display='none';
		document.getElementById("Date_error").style.display='none';
		document.getElementById("Email_not_match_error").style.display='block';
		Reg.remail.focus();
		return false;
	}
	

	var cpass=Reg.password.value;
	if(cpass.length<=5)
	{
		document.getElementById("error_design_format").style.display='block';
		document.getElementById("blank_error").style.display='none';
		document.getElementById("Name_error").style.display='none';
		document.getElementById("full_Name_error").style.display='none';
		document.getElementById("Email_error").style.display='none';
		document.getElementById("Email_not_match_error").style.display='none';
		document.getElementById("Gender_error").style.display='none';
		document.getElementById("Date_error").style.display='none';
		document.getElementById("Password_error").style.display='block';
		Reg.password.focus();
		return false;
	}
	
	
	var cgen=Reg.sex.value;
	if(cgen=="Select Sex:")
	{
		document.getElementById("error_design_format").style.display='block';
		document.getElementById("blank_error").style.display='none';
		document.getElementById("Name_error").style.display='none';
		document.getElementById("full_Name_error").style.display='none';
		document.getElementById("Email_error").style.display='none';
		document.getElementById("Email_not_match_error").style.display='none';
		document.getElementById("Password_error").style.display='none';
		document.getElementById("Date_error").style.display='none';
		document.getElementById("Gender_error").style.display='block';
		return false;
	}
	
	
	var cmon=Reg.month.value;
	if(cmon=="Month:")
	{
		document.getElementById("error_design_format").style.display='block';
		document.getElementById("blank_error").style.display='none';
		document.getElementById("Name_error").style.display='none';
		document.getElementById("full_Name_error").style.display='none';
		document.getElementById("Email_error").style.display='none';
		document.getElementById("Email_not_match_error").style.display='none';
		document.getElementById("Password_error").style.display='none';
		document.getElementById("Gender_error").style.display='none';
		document.getElementById("Date_error").style.display='block';
		return false;
	}
	
	
	var cday=Reg.day.value;
	if(cday=="Day:")
	{
		document.getElementById("error_design_format").style.display='block';
		document.getElementById("blank_error").style.display='none';
		document.getElementById("Name_error").style.display='none';
		document.getElementById("full_Name_error").style.display='none';
		document.getElementById("Email_error").style.display='none';
		document.getElementById("Email_not_match_error").style.display='none';
		document.getElementById("Password_error").style.display='none';
		document.getElementById("Gender_error").style.display='none';
		document.getElementById("Date_error").style.display='block';
		return false;
	}
	
	
	var cyear=Reg.year.value;
	if(cyear=="Year:")
	{
		document.getElementById("error_design_format").style.display='block';
		document.getElementById("blank_error").style.display='none';
		document.getElementById("Name_error").style.display='none';
		document.getElementById("full_Name_error").style.display='none';
		document.getElementById("Email_error").style.display='none';
		document.getElementById("Email_not_match_error").style.display='none';
		document.getElementById("Password_error").style.display='none';
		document.getElementById("Gender_error").style.display='none';
		document.getElementById("Date_error").style.display='block';
		return false;
	}
	
	return true;
}